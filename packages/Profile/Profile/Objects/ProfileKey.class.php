<?php
class ProfileKey{
	
	public $id;
	public $name;
	public $type;
	public $rangeMin = null;
	public $rangeMax = null;
	public $group = null;
	public $sortId;
	public $isEnabled;
	
}
