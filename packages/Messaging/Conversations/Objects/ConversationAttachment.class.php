<?php
class ConversationAttachment {
	public $id;
	public $messageId;
	public $systemFilename;
	public $filename;
	public $mimeType;
	public $isImage;
	public $date;
}
