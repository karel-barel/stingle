<?php
/**
 * DKIM Config Exception class
 * @author Arram Gevorgyan
 *
 */
class DKIMConfigException extends RuntimeException{ }
