<?php
$defaultConfig = array(	
						'AuxConfig' => array(	'uploadDir' => 'uploads/photos/data/',
												'saveFormat' => null,
												'acceptedMimeTypes' => array(	'image/gif',
																				'image/jpeg',
																				'image/pjpeg',
																				'image/png'
																			),
												'minimumSize' => array('largeSideMinSize'=> 50, 'smallSideMinSize' => 50)
											)
					);
