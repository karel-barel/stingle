<?php
$defaultConfig = array(	
						'AuxConfig' => array(	'cacheDir' => 'uploads/photos/cache/',
												'folders' => array(
																	'small',
																	'medium',
																	'big'
																)),
						'Objects' => array(	'ImageCache' => 'imageCache' )
					);
